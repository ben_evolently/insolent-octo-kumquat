package firebase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
)

type firebaseRoot struct {
	base_url   string
	auth_token string
}


/* Construct Firebase Type */
func New(url string) *firebaseRoot { 
	return &firebaseRoot{base_url: url} 
}


/* Writes The Data to Firebase Endpoint */

func (f *firebaseRoot) Set(path string, v interface{}) ([]byte, error) {
	b, err := json.Marshal(v)
	
	if err != nil { 
		return nil, err 
	}
	
	json := bytes.NewBuffer(b)

	body, err := f.SendRequest("PUT", path, json)
	
	if err != nil { 
		return nil, err 
	}

	return body, nil
}

/* Return Data at a Given Path */
func (f *firebaseRoot) Get(path string) ([]byte, error) {
	body, err := f.SendRequest("GET", path, nil)
	if err != nil { 
		return nil, err 
	}
	return body, nil
}
